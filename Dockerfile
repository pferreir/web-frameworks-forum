FROM ruby:2.4
MAINTAINER web-services-core <web-services-core@cern.ch>

RUN apt update
RUN apt install -y lsb-release wget
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -

# Install prerequisites #############################
RUN apt-get update && apt-get -y upgrade && \
    apt-get -y install gettext \
    # Needed by sidekiq
    gifsicle jpegoptim optipng jhead \
    # Needed by entrypoint
    postgresql-client-9.6

# Set up Discourse ##################################
ENV DISCOURSE_RELEASE v1.8.9
ENV RAILS_ENV production

RUN mkdir -p discourse

ENV RAILS_ROOT /discourse
ENV HOME /discourse

RUN git clone --depth=1 --single-branch --branch "$DISCOURSE_RELEASE"  https://github.com/discourse/discourse.git /discourse

WORKDIR $HOME
RUN mkdir -p ./public/plugins/ && \
    mkdir -p ./public/uploads/ && \
    mkdir -p ./tmp/sockets/ && \
    mkdir -p ./tmp/pids/

### Plugins
#   - OAuth
RUN git clone --depth=1 https://github.com/discourse/discourse-oauth2-basic.git /discourse/plugins/discourse-oauth2-basic

### Custom
#   - Logo
ADD ["images","/discourse/public/images/"]

RUN exec bundle install --deployment --without development:test
RUN exec bundle clean -V

ADD ["run-discourse.sh","run-sidekiq.sh","./"]
RUN chmod +x ./run-discourse.sh ./run-sidekiq.sh

RUN chmod -R a+rw /discourse

# entrypoint will be overriden for the sidekiq container
ENTRYPOINT ["./run-discourse.sh"]

EXPOSE 8080
