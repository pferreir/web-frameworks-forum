#!/bin/bash
# If there are files in /tmp/configmap that are not empty
# (overriden by a ConfigMap) copy them to /discourse/config
if [ -n "$(ls -A /tmp/discourse-configmap)" ]
then
  for f in /tmp/discourse-configmap/*
  do
    if [ -s $f ]
    then
      cp /tmp/discourse-configmap/* /discourse/config/
    fi
  done
fi

# Replace environment variables
echo "--> Overwritting env variables ..."
envsubst < /tmp/discourse-configmap/discourse.conf > /discourse/config/discourse.conf
echo "--> DONE"

echo "--> Running sidekiq ..."
RAILS_ENV=production bundle exec sidekiq -C config/sidekiq.yml